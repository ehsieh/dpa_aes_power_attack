/*! @file
	\brief T=0 declarations.

	T=0 is not implemented fully as library, but must be partly done in
	the commands itself. This reduces the RAM requirements. E.g. when
	doing an Update Binary the data must not be received in total before
	writing, but can be received and written in single bytes.

	$Id: t0.h,v 1.12 2002/12/22 15:42:55 m Exp $
*/

#ifndef SOSSE_T0_H
#define SOSSE_T0_H

/*! \brief Header of the current command.

	This global variable contains the header (CLA INS P1 P2 P3) of the
	current command. It is fetched in the command interpreter loop in
	main().
*/
extern uint8_t header[5];

/*! \brief Send ACK byte.

	Sends the for the current command correct ACK byte to the terminal.
*/
void t0_sendAck( void );
/*! \brief Send complemented ACK byte.

	Sends the for the current command correct complemented ACK byte to the
	terminal.
*/
void t0_sendCAck( void );
/*! \brief Send word in sw.

	Sends the word in sw the the terminal. This is used to return the
	status word at the end of the command interpreter loop in main().
*/


#endif /* SOSSE_T0_H */
