/* $Id: main.c,v 1.31 2002/12/24 13:33:11 m Exp $ */

/*! @file
 \brief main() function with command loop.
 */
#include "config.h"
#include "commands.h"
#include "hal.h"
#include "aes_unmask.h"


/*! \brief Main function containing command interpreter loop.

 At the end of the loop, sw is sent as the status word.

 This function does never return.
 */

/*! \brief Header of the current command.

	This global variable contains the header (CLA INS P1 P2 P3) of the
	current command. It is fetched in the command interpreter loop in
	main().
*/
//uint8_t header[5+16];
//(uint8_t*) 0x0300;
#define RAND_MAX 0x7FFF
void my_init_stack (void) __attribute__ ((naked)) __attribute__ ((section (".init2")));
void my_init_stack (void) {
	SPH = 0x04;
	SPL = 0x00;
}

//Args o crypto unctions passed directly to ixed adresses

uint8_t __msg__[] 			= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t __key__[] 			= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};// The actual encryption key used is set inside the assembly encryption routine 
																															   // TODO:Store the key in the internal eeprom

#define decl_shuffle(type)							\
void shuffle_##type(type *list, size_t len) {		\
	int j;											\
	type tmp;										\
	while(len) {									\
		j = irand(len);								\
		if (j != len - 1) {							\
			tmp = list[j];							\
			list[j] = list[len - 1];				\
			list[len - 1] = tmp;					\
		}											\
		len--;										\
	}												\
}													\

/* random integer from 0 to n-1 */
int irand(int n)
{
	int r, rand_max = RAND_MAX - (RAND_MAX % n);
	/* reroll until r falls in a range that can be evenly
	 * distributed in n bins.  Unless n is comparable 
	 * to RAND_MAX, it's not *that* important really. */
	while ((r = rand()) >= rand_max);
	return r / (rand_max / n);
}
 
/* declare and define int type shuffle function from macro */
decl_shuffle(uint8_t);
 

#if defined(CTAPI)
void sosse_main( void )
#else
int main(void)
#endif
{
	WDTCR = 0x10;
	static uint8_t i, len, b , g ,tmp;
	static uint8_t * __o__ = (uint8_t*) 0x0300;
	uint8_t * __header__ = (uint8_t*) 0x0320;
	uint8_t * __suffle0__ = 0x0280;
	uint8_t * __suffle10__ = 0x0230;
	
	/* TODO: On error? */
	hal_init();

	if (!(hal_eeprom_read(&len, ATR_LEN_ADDR, 1) && (len <= ATR_MAXLEN)))
	for (;;) {
	}

	for (i = 1; i < len; i++) {
		if (!hal_eeprom_read(&b, ATR_ADDR + i, 1))
		for (;;) {
		}
		hal_io_sendByteT0(b);
	}
	/* Command loop */
	for (;;) {
		__header__[4]= 0x00;
		__header__[3]= 0x00;
		__header__[2]= 0x00;
		__header__[1]= 0x00;
		__header__[0]= 0x00;

		for (i = 0; i < 16; i++) __suffle0__[i] = i;
		for (i = 0; i < 16; i++) __suffle10__[i] = i;
		
		for (i = 0; i < 5+32; i++) {
			__header__[i] = hal_io_recByteT0();
		}
		
		hal_rnd_addEntropy();
		
		if ((__header__[0] & 0xFC) == CLA_PROP) {
			switch (__header__[1] & 0xFE) {
				case INS_GET_RESPONSE:
					hal_rnd_getBlock(__o__);

					t0_sendAck();
					for (g = 0;  g < 16;  g+=1) {
						hal_io_sendByteT0(__o__[g]%16 );	
					}
					t0_sendAck();

					for (g = 0; g <16; g++) {
						__key__[g]=__header__[5+g];
					}					
					for (g = 0; g < 16; g++) {
						__msg__[g] = __header__[21+g];
					}

					for (g = 5;  g < 5+ 32;  g+=1) {
						hal_io_sendByteT0(__header__[g] );	
					}

					
					shuffle_uint8_t(__suffle0__, 16);
					shuffle_uint8_t(__suffle10__, 16);

					t0_sendAck();

					for (g = 0;  g < 16;  g+=1) {
						hal_io_sendByteT0(__suffle0__[g]%16 );	
					}
					for (g = 0;  g < 16;  g+=1) {
						hal_io_sendByteT0(__suffle10__[g]%16 );	
					}	

					t0_sendAck();
					aes_unmask_enc(__msg__,__key__,__o__,0,1); //Encrypt challenge

				break;
				default:
					asm ("nop"); //TODO : make it compatible with pcsc
			}
			} else {
			asm ("nop");
		}

	}
}
