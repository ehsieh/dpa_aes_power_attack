/* aes-rsm.h */

#ifndef AESRSM_COMMON_H_
#define AESRSM_COMMON_H_

#include <stdint.h>

#define AES_KEY_LEN	16	//!< AES 128 key size.
#define AES_BLOCK_LEN	16	//!< AES 128 block length.



void aes_rsm_enc(void);

void aes_rsm_cenc(uint8_t*v,uint8_t*k,uint8_t *j,uint8_t rng, uint8_t plot)
;

void aes_rsm_core_cenc( void );

#endif /*AESRSM_H_*/
