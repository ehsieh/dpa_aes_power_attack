/* aes-rsm.c */

#include <string.h>
#include <stdint.h> 
#include <avr/pgmspace.h>
#include "aes-rsm.h"
#include "hal.h"

void aes_rsm_enc(void){
 	aes_rsm_core_cenc();
	return;
}

void aes_rsm_cenc(uint8_t*v,uint8_t*k,uint8_t *j,uint8_t trng, uint8_t plot)
 {
	uint8_t *__data__		= (uint8_t*) 0x0240;
	uint8_t *__key__		= (uint8_t*) 0x0250;
	uint8_t * __header__	= (uint8_t*) 0x0320;
	uint8_t * __indi__		= (uint8_t*) 0x0270;
	uint8_t * __suffle0__	= (uint8_t*) 0x0280;
	uint8_t * __suffle1__	= (uint8_t*) 0x0230;
	uint8_t * __pok__		= (uint8_t*) 0x0290;
	uint8_t * __offset__	= (uint8_t*) 0x0300;
	uint8_t * __reg_out__	= (uint8_t*) 0x0310;
	uint8_t i;
	
	for(i=0;i<16;i++){
		__key__[i] = k[i];
	}
	
	for(i=0;i<16;i++){
		__data__[i] = v[i];
	}
	//Start encryption
	trng?:hal_trig1();
	aes_rsm_enc();
	trng?:hal_trig0();	
	
	//Adresses of tables are staticaly declared so than to ease SCA anaylis.
	
	for(i=0;i<16;i++){
		v[i] = __data__[i];
	}

		//Output cyphertext
	if(!trng){
		if(plot){
		/* ACK */
		t0_sendAck();
		for (i = 0;  i < 16;  i+=1) {
			hal_io_sendByteT0(v[i] );	

		}
		
		t0_sendAck();
		}
	}
}

