#-*- coding: cp1252 -*-
# This code is part of DPAV4 project
# Copyright 2013 Telecom ParisTech

import sys
import os
import threading
import time
import logging
import math
import serial
import glob
class SmartcardSerial():
    
    def __init__(self):
        self.ser = None

    def __del__(self):
        self.close()
    
    def connect(self, serport):
        if self.ser == None:
            # Open serial port 
            self.ser = serial.Serial()
            self.ser.port     = serport
            self.ser.baudrate = 9600
            self.ser.timeout  = 3     # 3 second timeout
            self.ser.stopbits = serial.STOPBITS_TWO
            self.ser.parity = serial.PARITY_EVEN
            self.ser.open()
        
        self.reset()

    def reset(self):
        self.ser.flush()
        self.ser.setRTS(True)
        time.sleep(0.2)
        self.ser.setRTS(False)
        time.sleep(0.5)

        data = bytearray(self.ser.read(self.ser.inWaiting()))
        stratr = "ATR = "
        for p in data:
            stratr = stratr + "%2X "%p

        print stratr
        return stratr
    
    def disconnect(self):
        self.close()
            
    def close(self):
        if self.ser != None:
            self.ser.close()
            self.ser = None
        
    def init(self):
        self.reset()

    def getATR(self):
        return self.stratr

    def APDUHeader(self, cla, ins, p1, p2, datalen):
        header = bytearray()
        header.append(cla)
        header.append(ins)
        header.append(p1)
        header.append(p2)
        header.append(datalen)
	header=str(header)
        self.ser.flushInput()
        self.ser.write(header)
        
        try:
            resp = bytearray(self.ser.read(2))
            print "readed"
            if (resp[1] != ins):
                print "SASEBOW: APDUHeader Sync Error %X != %X"%(resp[1], ins)
                return False
        except:
            print "SASEBOW: APDUHeaderSync Error, no response"
            return False

        return True

    def APDUSend(self, cla, ins, p1, p2, data):
        self.APDUHeader(cla, ins, p1, p2, len(data))   
        self.ser.write(str(bytearray(data)))

        #For non-sasebow, get rid of echo
        self.ser.read(len(data))
        
        resp = self.ser.read(2)
        print "read data"

        if len(resp) < 2:
            print "SASEBOW: APDUSend Data Error, no response"

        return True

    def APDURecv(self, cla, ins, p1, p2, datalen):
        self.APDUHeader(cla, ins, p1, p2, datalen)
        
        resp = self.ser.read(datalen + 2)

        if len(resp) < (datalen + 2):
            print "SASEBOW: APDUResp Data Error, response too short"
            return 0

        resp = bytearray(resp)
        
        return resp[0:datalen]        
                    
    def setModeEncrypt(self):
        return

    def setModeDecrypt(self):
        return

    def loadEncryptionKey(self, key):
        if key != None:
            self.APDUSend(0x80, 0x12, 0x00, 0x00, key)
      
    def loadInput(self, inputtext):
        self.input = inputtext

    def isDone(self):
        return True

    def readOutput(self):
        return self.APDURecv(0x80, 0xC0, 0x00, 0x00, 16)

    def enc(self,datai,test):
        if(test==1):
            self.APDUSend(0x80,0xC0,0x00,0x00,datai) #datai 
            print "hello"
            time.sleep(0.5)
            data = bytearray(self.ser.read(103))
            buf = ""
            for p in data:
                buf = buf + "%02X"%p
            self.ser.flush()
            print buf
            return buf  
        if(test==0) :
            self.APDUSend(0x80,0xC0,0x00,0x00,datai) #datai
            time.sleep(0.5)
            self.ser.flush()
            
        
    def go(self):
        self.APDUSend(0x00, 0x04, 0x04, 0x00, self.input)

def main():
    test=SmartcardSerial()
    port='/dev/ttyUSB0'
    test.connect(port)
    #datav = open("./plainv42_5000.csv", "r") 
    datav = open("./pt_samples", "r") 
    for i in range(100):
        datai=datav.readline()[:-1]
        datai=datai.decode('hex')
        test.enc(datai,1)
   
if __name__ == "__main__":
    main()     
