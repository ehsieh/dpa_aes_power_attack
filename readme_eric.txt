Download and install FTDI and Avrdude:
Eric >>> Reference to http://doswa.com/2010/08/24/avrdude-5-10-with-ftdi-bitbang.html

sudo apt-get install patch build-essential libreadline-dev libncurses-dev libusb-dev libftdi-dev automake autoconf bison flex
sudo apt-get build-dep avrdude avrdude-doc

cd /
mkdir avrdude
cd avrdude

# For 64-bit:
wget http://www.ftdichip.com/Drivers/D2XX/Linux/libftd2xx0.4.16_x86_64.tar.gz
# For 32-bit:
wget http://www.ftdichip.com/Drivers/D2XX/Linux/libftd2xx0.4.16.tar.gz

# Get Avrdude-5.10
wget http://download.savannah.gnu.org/releases-noredirect/avrdude/avrdude-5.10.tar.gz

# Get patches
for i in 8 7 6 5 4 3 2 1 0; do wget -O patch-$i.diff http://savannah.nongnu.org/patch/download.php?file_id=1851$i; done

# Untar, patches
tar xzf avrdude-5.10.tar.gz
tar xzf libftd2xx*.tar.gz
cd avrdude-5.10
for file in ../patch-*.diff; do patch -p0 < $file; done
cp ../libftd2xx0.4.16_x86_64/static_lib/* .
cp ../libftd2xx0.4.16_x86_64/*.h .
cp ../libftd2xx0.4.16_x86_64/*.cfg .

# Install lex in flex and yacc in bison
sudo apt-get install flex bison

# Configure and make
./configure CFLAGS="-g -O2 -DSUPPORT_FT245R" LIBS="./libftd2xx.a.0.4.16 -lpthread -ldl -lrt"
make

# If evrything is ok, you can now install avrdude
sudo make install


Old stuff:
=====================================================================================================
Assuming you are under Linux Ubuntu or other similar OS (eg. Debian), if you want to flash the smart card directly though the USB port of the SASEBO-W board, you have to recompile avrdude with the FTDI FT245 driver. Try the folowing steps :

- First, install the dependencies that are necessary to use FTDI chip. On Ubuntu 10.04, this should take care of all of them:

sudo apt-get install patch build-essential libreadline-dev libncurses-dev libusb-dev libftdi-dev automake autoconf bison flex
sudo apt-get build-dep avrdude avrdude-doc

- Now download the D2xx driver from FTDI. Only download the one that corresponds to your OS (either 64 bit or 32 bit) :

# For 64-bit:
wget http://www.ftdichip.com/Drivers/D2XX/Linux/libftd2xx0.4.16_x86_64.tar.gz
# For 32-bit:
wget http://www.ftdichip.com/Drivers/D2XX/Linux/libftd2xx0.4.16.tar.gz

- Extract everything, put the drivers into place (in /avrdude), and apply the patches:

tar xf libftd2xx*.tar.gz
cd avrdude-5.10
for file in ../patch-*.diff; do patch -p0 < $file; done
cp ../libftd2xx*/static_lib/* .
cp ../libftd2xx*/*.h .
cp ../libftd2xx*/*.cfg .


- Configure and compile AVRDUDE.

./configure CFLAGS="-g -O2 -DSUPPORT_FT245R" LIBS="./libftd2xx.a.0.4.16 -lpthread -ldl -lrt"
make

- If evrything is ok, you can now install avrdude :

# If you're using Ubuntu, you can use checkinstall to build a .deb package and install it:
sudo checkinstall
# Otherwise, just use `make install`:
sudo make install

------------------------------------------------------

Now, you can properly flash the smart-card. To do that , the FTDI 2232 chip have to be reconfigured in FIFO 245 mode:

# Go into FTDI/write/ , and reconfigure the FTDI chip :
make clean && make .
sudo rmmod ftdi_sio && usbserial .
./write -p 0 -m run

#You shoud have the folowing output :
./write -p 0 -m pgm
0x1 Mode = pgm (fifo FT245 mode) please set SW3 [1,2,3,4] to 'ON'
opening port 0
ftHandle0 = 0x84622b8


-Set the SW3 switches as explained above .

- Then, you can flash the smart card :

# CD into /smart-dpav4/ where dpa4.hex as well as eeprom.hex are stored and program the smart-card :

sudo avrdude -c sasebow -p m163 -P ft0 -B 115200 -u -e -Uflash:w:dpa4.hex:a -Ueeprom:w:eedata.hex:a -U lfuse:w:0xd0:m -U hfuse:w:0xfe:m

# You should have the folowing output :

xd0:m -U hfuse:w:0xfe:m
[sudo] password for zak:
avrdude: BitBang OK
avrdude: pin assign miso 3 sck 4 mosi 0 reset 6
avrdude: drain OK

 ft245r:  bitclk 76800 -> ft baud 38400
avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.00s

avrdude: Device signature = 0x1e9402
avrdude: erasing chip
 ft245r:  bitclk 76800 -> ft baud 38400
avrdude: reading input file "dpa4.hex"
avrdude: input file dpa4.hex auto detected as Intel Hex
avrdude: writing flash (12780 bytes):

Writing | ################################################## | 100% 2.77s



avrdude: 12780 bytes of flash written
avrdude: verifying flash memory against dpa4.hex:
avrdude: load data flash data from input file dpa4.hex:
avrdude: input file dpa4.hex auto detected as Intel Hex
avrdude: input file dpa4.hex contains 12780 bytes
avrdude: reading on-chip flash data:

Reading | ################################################## | 100% 1.07s



avrdude: verifying ...
avrdude: 12780 bytes of flash verified
avrdude: reading input file "eedata.hex"
avrdude: input file eedata.hex auto detected as Intel Hex
avrdude: writing eeprom (138 bytes):

Writing | ################################################## | 100% 0.47s

avrdude: 138 bytes of eeprom written
avrdude: verifying eeprom memory against eedata.hex:
avrdude: load data eeprom data from input file eedata.hex:
avrdude: input file eedata.hex auto detected as Intel Hex
avrdude: input file eedata.hex contains 138 bytes
avrdude: reading on-chip eeprom data:

Reading | ################################################## | 100% 0.05s

avrdude: verifying ...
avrdude: 138 bytes of eeprom verified
avrdude: reading input file "0xd0"
avrdude: writing lfuse (1 bytes):

Writing |                                                    | 0% 0.00s ***failed;
Writing | ################################################## | 100% 0.01s

avrdude: 1 bytes of lfuse written
avrdude: verifying lfuse memory against 0xd0:
avrdude: load data lfuse data from input file 0xd0:
avrdude: input file 0xd0 contains 1 bytes
avrdude: reading on-chip lfuse data:

Reading | ################################################## | 100% 0.00s

avrdude: verifying ...
avrdude: verification error, first mismatch at byte 0x0000
         0xd0 != 0xc0
avrdude: verification error; content mismatch

avrdude done.  Thank you.
